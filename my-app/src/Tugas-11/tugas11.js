import React, { useState, useEffect } from 'react';
import '../App.css';

function App() {
    function Clock({ template }) {
  
        var timer;
      
        function render() {
          var date = new Date();
      
          var hours = date.getHours();
          if (hours < 10) hours = '0' + hours;
      
          var mins = date.getMinutes();
          if (mins < 10) mins = '0' + mins;
      
          var secs = date.getSeconds();
          if (secs < 10) secs = '0' + secs;
      
          var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
      
          return (
              <div>{output}</div>
          )
        }
      
        this.stop = function() {
          clearInterval(timer);
        };
      
        this.start = function() {
          render();
          timer = setInterval(render, 1000);
        };
      
      }

    function runClock(){
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
      
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
      
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var tanda = "AM";
        
        if(hours>12){
            hours -= 12;
            tanda = "PM"
        }
        return (
            <h2>{`${hours}:${mins}:${secs} ${tanda}`}</h2>
        )
    }

    const [seconds, setSeconds] = useState(0);
    const [mundur, setMundur] = useState(20);

    useEffect(() => {
        const interval = setInterval(() => {
        setSeconds(() => runClock());
        }, 1000);
        
        return () => clearInterval(interval);
    }, []);

    useEffect(() => {
        if(mundur>0){
            const interval_mundur = setInterval(() => {
            setMundur(mundur-1)
            }, 1000);
            return () => clearInterval(interval_mundur);
        }
    }, [mundur]);

  return (
    <div>
        {
            (mundur>0) ? 
                <div>
                    <header>
                    <div style={{display: "flex", flexDirection: "row", justifyContent:"space-between"}}>
                        <div style={{display: "flex", flexDirection: "row"}}>
                            <h2 style={{marginRight: "10px"}}>sekarang jam :</h2>
                            {(seconds!=0) ?
                                <div>{seconds}</div>

                                :
                                null
                            }
                            
                        </div>
                        <div style={{display: "flex", flexDirection: "row"}}>
                            <h2 style={{marginRight: "10px", marginLeft:"10px"}}>Hitung Mundur :</h2>
                            {<h2>{mundur}</h2>}
                        </div>
                    </div>
                    </header>
                    <body>
                    
                    </body>   
                </div>
        
              : null
            
        }
    </div>
    
  );
}

export default App;