import React, {Component, useState, useEffect, createContext, useContext} from 'react';
import '../App.css';
import axios from 'axios';
import {Context} from "./main.js"

function Tugas(props) {

    const [data, setData] = useContext(Context)
    const [form, setForm] = useState(-1);

    console.log(data)
    const Edit = (e) => {
        e.preventDefault();
        setForm(e.target.value)
    }

    const Delete = (e) => {
        e.preventDefault();
        console.log(e.target.value);
        var temp_array = data.filter(function(value,index){
            return index!=e.target.value;
        });
        console.log(temp_array);
        setData(temp_array)
    }
    
    return (
        <div className="App">
            <header className="App-header">
                <h1>Tabel Harga Buah</h1>
            </header>
            <body>
                <div style={{display: "flex", justifyContent:"center"}}>
                    <table style={{width: "50%", border: "1px"}}>
                        <tr style={{backgroundColor: "#BEBEBE"}}>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>
                        {data.map((value,index) => {
                                return (
                                    <tr>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.nama}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.harga}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.berat}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>
                                            <form>
                                                <button value={index} onClick={(e) => Edit(e)}>Edit</button>
                                                <button value={index} onClick={(e) => Delete(e)}>Delete</button>
                                            </form>
                                            {
                                                (form==index) ?
                                                    <div>
                                                        <form>
                                                            <input type="text">
                                                            </input>
                                                            <input type="text" ></input>
                                                            <input type="text" ></input>
                                                        </form>
                                                    </div>
                                                :
                                                null
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </table>
                </div>
            </body>   
        </div>
    );
  }
  
  Tugas.getInitialProps = async function() {
    const res = await fetch
    ("http://backendexample.sanbercloud.com/api/fruits");
    const data = await res.json();

    return{
        dataBuah: data
    };
}

  export default Tugas