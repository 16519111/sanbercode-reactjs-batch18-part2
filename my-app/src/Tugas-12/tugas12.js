import React, {Component} from 'react';
import '../App.css';

export default class List extends Component {

    constructor(props){
        super(props);
        this.state = {
            dataHargaBuah : [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ],
            form : 0,
            currentIndex : 0
        }
    }

    Edit(e){
        e.preventDefault();
        this.setState({form: e.target.value})
    }
    Delete(e){
        e.preventDefault();
        console.log(e.target.value);
        var temp_array = this.state.dataHargaBuah.filter(function(value,index){
            return index!=e.target.value;
        });
        console.log(temp_array);
        this.setState({dataHargaBuah: temp_array});
    }
    render() {
        return (
        <div className="App">
            <header className="App-header">
                <h1>Tabel Harga Buah</h1>
            </header>
            <body>
                <div style={{display: "flex", justifyContent:"center"}}>
                    <table style={{width: "50%", border: "1px"}}>
                        <tr style={{backgroundColor: "#BEBEBE"}}>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>
                        {this.state.dataHargaBuah.map((value,index) => {
                                return (
                                    <tr>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.nama}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.harga}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>{value.berat}</td>
                                        <td style={{backgroundColor: "#FF603B"}}>
                                            <form>
                                                <button value={index} onClick={(e) => this.Edit(e)}>Edit</button>
                                                <button value={index} onClick={(e) => this.Delete(e)}>Delete</button>
                                            </form>
                                            {
                                                (this.state.form==index) ?
                                                    <div>
                                                        <form>
                                                            <input type="text">
                                                            </input>
                                                            <input type="text" onChange={(e) => this.GantiHarga(e)}></input>
                                                            <input type="text" onChange={(e) => this.GantiBerat(e)}></input>
                                                        </form>
                                                    </div>
                                                :
                                                null
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </table>
                </div>
            </body>   
        </div>
        )
      }
    
}