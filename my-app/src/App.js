import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Tugas_9 from './Tugas-9/tugas9';
import Tugas_10 from './Tugas-10/tugas10';
import Tugas_11 from './Tugas-11/tugas11';
import Tugas_12 from './Tugas-12/tugas12';
import Tugas_13 from './Tugas-13/tugas13';
import Tugas_14 from './Tugas-14/control';

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/Tugas9">Tugas 9</Link>
            </li>
            <li>
              <Link to="/Tugas10">Tugas 10</Link>
            </li>
            <li>
              <Link to="/Tugas11">Tugas 11</Link>
            </li>
            <li>
              <Link to="/Tugas12">Tugas 12</Link>
            </li>
            <li>
              <Link to="/Tugas13">Tugas 13</Link>
            </li>
            <li>
              <Link to="/Tugas14">Tugas 14</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/Tugas9">
            <Tugas_9 />
          </Route>
          <Route path="/Tugas10">
            <Tugas_10 />
          </Route>
          <Route path="/Tugas11">
            <Tugas_11 />
          </Route>
          <Route path="/Tugas12">
            <Tugas_12 />
          </Route>
          <Route path="/Tugas13">
            <Tugas_13 />
          </Route>
          <Route path="/Tugas14">
            <Tugas_14 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

