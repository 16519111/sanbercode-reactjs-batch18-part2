import React from 'react';
import '../App.css';

function App() {
    let dataHargaBuah = [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ]

  return (
    <div className="App">
      <header className="App-header">
        <h1>Tabel Harga Buah</h1>
      </header>
      <body>
        <div style={{display: "flex", justifyContent:"center"}}>
            <table style={{width: "50%"}}>
                <tr style={{backgroundColor: "#BEBEBE"}}>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                </tr>
                {dataHargaBuah.map((value) => {
                        return (
                            <tr style={{backgroundColor: "#FF603B"}}>
                                <td>{value.nama}</td>
                                <td>{value.harga}</td>
                                <td>{value.berat}</td>
                            </tr>
                        )
                    })
                }
            </table>
        </div>
      </body>   
    </div>
    
  );
}

export default App;