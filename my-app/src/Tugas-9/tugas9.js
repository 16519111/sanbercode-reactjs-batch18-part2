import React from 'react';
import '../App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Form Pembelian Buah</h1>
      </header>
      <body>
        <div>
            <div class="grid-container">
                <div class="item1"><strong>Nama Pelanggan</strong></div>
                <div class="item2"><input type="text"/></div>
                <div class="item3"></div>
            </div>
            <div class="grid-container">
                <div class="item1"><strong>Daftar Item</strong></div>
                <div class="item2">
                    <div>
                      <input type="checkbox" id="Semangka" name="gender" value="Semangka"/>
                      <label for="Laki-laki">Semangka</label>
                    </div>
                    <div>
                      <input type="checkbox" id="Jeruk" name="gender" value="Jeruk"/>
                      <label for="Laki-laki">Jeruk</label>
                    </div>
                    <div>
                      <input type="checkbox" id="Nanas" name="gender" value="Nanas"/>
                      <label for="Laki-laki">Nanas</label>
                    </div>
                    <div>
                      <input type="checkbox" id="Salak" name="gender" value="Salak"/>
                      <label for="Laki-laki">Salak</label>
                    </div>
                    <div>
                      <input type="checkbox" id="Anggur" name="gender" value="Anggur"/>
                      <label for="Laki-laki">Anggur</label>
                    </div>
                </div>
                <div class="item3"></div>
            </div>
            <div class="grid-container">
                <div class="item1"><input type="submit" value="Kirim" style={{borderRadius: '15px'}}/></div>
            </div>
        </div>
      </body>   
    </div>
    
  );
}

export default App;